JavaScript and TypeScript clients for SignalR for ASP.NET Core

## Installation

```bash
bower install https://bitbucket.org/nuba/signalr-client.git
```

## Usage

### Example

```JavaScript
let connection = new signalR.HubConnection('/chat');

connection.on('send', data => {
    console.log(data);
});

connection.start()
    .then(() => connection.invoke('send', 'Hello'));
```